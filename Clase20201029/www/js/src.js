'use strict';

// IIFE

//la función está entre parentesis y luego hay dos más para parametros
//la función se "auto llama"
(function sayHello(name){
    console.log(`Hola ${name}`);
})('Rubén');

//funcion anónima
(function (name){
    console.log(`Hola ${name}`);
})('Rubén');
//utilidad limitada a día de hoy

//OBJETO GLOBAL WINDOW
//visualización del objeto window
console.log(window);
//visualización de un metodo del objeto window
console.log(window.console);
//visualización de metodos y propiedades del objeto window
console.log(window.console.warn);
console.log(window.console.memory);

console.log(this);


//Arrays, pueden llevar cualquier cosa dentro y no del mismo tipo, tipo tuplas

//declaración de un array vacio

const array = [];

const myArray =[true, 54, 'Daniel',[3,5]];

//visualización de posiciones

console.log(myArray[0]);
console.log(myArray[1]);
console.log(myArray[2]);
console.log(myArray[3]);
console.log(myArray[3][0]);
console.log(myArray[3][1]);

//asignación a una posición

myArray[0] = false;
console.log(myArray);
// myArray = [true, 4, 3, 'sas'];

//bucle for-of solo permite acceder a los valores de array
for (const value of myArray) {
    console.log(value)
}

//metodo push en arrays

const colores = ['rojo', 'verde', 'naranja', 'azul']
console.log(colores);
colores.push('rosa');
console.log(colores);
