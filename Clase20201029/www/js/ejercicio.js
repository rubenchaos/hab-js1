'use strict';

const array = [1, 3, 9, 14, 17, 22];
let i=0;

console.log('iteración de contenido con while');
while (i<array.length){
    console.log(array[i]);
    i++;
}

console.log('iteración de contenido con for');
for (i=0; i < array.length;i++){
    console.log(array[i]);
}

console.log('iteración de contenido con for-of');
for (const value of array){
    console.log(value);
}

console.log('mostrar todos los elementos sumandole 1');
for(let value of array){
    console.log(value += 1);
}

console.log('array apartir del primero incrementado en 1');
let array2 = [];
for (let i = 0; i<array.length; i++){
    array2.push(array[i]+1);
}
console.log(array2);

console.log('promedio');
let suma = 0;
for (const value of array){
    suma += value;
}
console.log(suma/array.length);