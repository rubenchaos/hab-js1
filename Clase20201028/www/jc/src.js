'use strict';
//Expresión de función

const calculate = function(firstNum, secondNum){
    return firstNum + secondNum;
}


//Arrow function (función flecha) sin return implicito
const calculate2 = (firstNum, secondNum) => {
    return firstNum + secondNum;
}

//Arrow function (función flecha) con return implicito, solo para funciones que requieren de una línea de código
const calculate3 = (firstNum, secondNum) => firstNum + secondNum;


const userName1 = 'Fulano';
const userAge1 = 34;

//función declarada
function showInfo1(name, age){
    return `El nombre de usuario es ${name} y su edad es ${age}`;
}

//expresión de función
const showInfo2 = function (name, age){
    return `El nombre de usuario es ${name} y su edad es ${age}`;
}

//arrow function no implicito
const showInfo3 = (name, age) => {
    return `El nombre de usuario es ${name} y su edad es ${age}`;
}

//arrow function implicito
const showInfo4 = (name, age) => `El nombre de usuario es ${name} y su edad es ${age}`;

console.log(showInfo1(userName1,userAge1));
console.log(showInfo2(userName1,userAge1));
console.log(showInfo3(userName1,userAge1));
console.log(showInfo4(userName1,userAge1));