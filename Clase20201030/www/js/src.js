'use strict';

const myText = 'Hola a TODOS los presentes.'

//propiedad length
console.log(myText.length);

//metodo toLoweCase() --> todo a minusculas
console.log(myText.toLowerCase());

//metodo toUpperCase() --> todo a mayúsculas
console.log(myText.toUpperCase());

//indexOf() devuelve el ÍNDICE del primer caracter que coincida con el dado
console.log(myText.indexOf('l'));
console.log(myText.indexOf('ñ')); //si no encuentra el caracter devuelve -1
console.log(myText.indexOf('TODOS')); //de un conjunto de caracteres te da la posición del prime caracter de la secuencia
console.log(myText.indexOf('o', 3))

//lastIndexOf() es el caso contrario, da el ÍNDICE del último caracter que coincida con el dado
console.log(myText.lastIndexOf('l'));
console.log(myText.lastIndexOf('ñ'));

//repeat(n) repite n veces el texto
console.log(myText.repeat(4));

//replace() busca una coincidencia y la reemplaza por lo que tu le digas, solo reemplaza un elemento
console.log(myText.replace('TODOS', 'todos'));

//replaceAll() reemplaza todas las coincidencias por lo que tu le marques
console.log(myText.replaceAll('e','a'));

//split() genera un array con el corte por donde se le indique en 
console.log(myText.split(' '));//array de palablas
console.log(myText.split(''));//array de letras
const date = '30/10/2020'
console.log(date.split('/'));
const time = '19:31:00'
console.log(time.split(':'));

//lo anterior con destructuring de arrays
const [hour, minutes, seconds] = time.split(':');
console.log(hour);
console.log(minutes);
console.log(seconds);

//charAt() te da el caracter correspondiente a un determiando índice
console.log(myText.charAt(13));

//substring() nos da una string extraido de un string entre dos índices
console.log(myText.substring(3,10));
console.log(myText.substring(8)); //desde index = 8 hasta el final

let example = 'tortilla';
console.log(example.split('').reverse().join(''));
//lo hecho es obtener un array con cada una de las letras de 'tortilla' para poder aplicar el metodo reverse que invierte un array
//OJO SOLO APLICABLE reverse  A ARRAYS, luego el metodo join que une un array en un string

//metodos con arrays, ver notas


//visualizacion de la clase String en la que se engloban los strings
// console.log(String.prototype);