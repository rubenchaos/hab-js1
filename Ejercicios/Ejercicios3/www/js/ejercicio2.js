/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Utiliza el método filter para dejar pasar únicamente los nº impares y mayores que 5.
 *
*/


'use strict';

console.log('EJERCICIO 2');
const numbers = [1, 5, 23, 4, 12, 45, 78, 8, 9, 10, 11];

console.log(numbers.filter( value => (value%2 !==0) && (value > 0)));