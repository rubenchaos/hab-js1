/* ################
 * ## Pirámide 2 ##
 * ################
 *
 * Utiliza el bucle "for" para crear las siguiente figura con asteriscos (*). Debe existir 
 * una variable que permita modificar la altura de la pirámide. Para el ejemplo expuesto a 
 * continuación se ha tomado como referencia una altura de 4: 
 * 
 * - Figura 2:
 * 
 *        *
 *       **
 *      ***
 *     ****
*/

'use strict';
//declaración de variables
let piramideHeight = 5;  //altura de la piramide
let spaces = "";         //variable para acumulación de espacios
let stars = "";          //variable para acumulación de asteriscos

//construcción de la piramide
for (let i=piramideHeight;i>0;i--){ //for externo recorre filas y genera la acumulación de asteriscos
    spaces = "";                         //reinicio de la acumulación de espacios en cada paso
     for (let j=1;j<i;j++){
         spaces += " "
     }
    stars += "*";                       //acumulación de asteriscos
    console.log(spaces+stars);
}
