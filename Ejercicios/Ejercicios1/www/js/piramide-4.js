/* ################
 * ## Pirámide 4 ##
 * ################
 *
 * Utiliza el bucle "for" para crear las siguiente figura con asteriscos (*). Debe existir una
 * variable que permita modificar la altura de la pirámide. Para el ejemplo expuesto a 
 * continuación se ha tomado como referencia una altura de 4: 
 * 
 * - Figura 4:
 * 
 *        *
 *       ***
 *      *****
 *     *******
 *      *****
 *       ***
 *        *
*/

'use strict';
//declaración de variables
let piramideHight = 10; //altura de medio rombo
let spaces = "";        //variable para acumulación de espacios
let stars = "*";        //variable para acumulación de asteriscos

//construcción de la parte superior del rombo
for (let i=piramideHight;i>0;i--){  //for externo para recorrido por filas y consturccion de espacios en blanco
    spaces = "";                    //reinicio de la acumulación de espacios en cada paso
     for (let j=1;j<=i;j++){        //for interno para acumulacion de espacios en cada piso
         spaces += " "
     }
    console.log(spaces+stars);
    stars += "**";                  //acumulación de asteriscos
}
spaces = "  ";

//construcción de la parte inferior del rombo, lógica inversa al anterior con un piso menos
for(let i=(piramideHight-1);i>0;i--){
    stars="";
    for (let j=1;j<=2*i-1;j++){
        stars += "*";
    }
    console.log(spaces+stars);
    spaces += " ";
}