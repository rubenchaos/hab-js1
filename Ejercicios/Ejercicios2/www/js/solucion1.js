'use strict';

//determina si un numero es divisible entre otro por cocientes
const isDivisible = function(numerator,quotient){
    if(numerator % quotient === 0){
        return true;
    } else{
        return false;
    }
}

//determina si un numero es primo por cocientes
const isPrimeNumber = function(number){
    let result = false;
    for (let i=number-1 ; i>2; i--){
        if (isDivisible(number, i)){
            result = false;
            break;
        } else {
            result = true;
        }
    }
    return result;
}

//determina los numeros primos menores a uno dado por cocientes
const createPrimeNumberList = function(number){
    for (let i=number; i>1; i--){
        if (isPrimeNumber(i)){
            console.log(i);
        }
    }
}

let numero = 1000;

createPrimeNumberList(numero);