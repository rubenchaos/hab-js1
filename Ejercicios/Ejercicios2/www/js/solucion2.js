'use strict';

//ESTASOLUCION PLATEA UNA PEQUEÑA CRIBA TIPO CRIBA DE ERATOSTENES

//Determina si un numero es múltipo de 2, si es así da un true
const isEven = function (number){
    const units = number % 10;
    if (units === 2 || units === 4 || units === 6 || units === 8 || units ===0){
        return true;
    } else {
        return false;
    }
}

//Determina si un numero es múltiplo de 5, si es así da un true
const isMultiplyFive = function (number){
    const units = number % 10;
    if (units === 5){
        return true;
    } else {
        return false;
    }
}

//Determina si un numero es múltiplo de 3, si es así da un true
const isMultiplyThree = function (number){
    if (number % 3 === 0){
        return true;
    } else {
        return false;
    }
}

//Determina si un numero es múltiplo de 7, si es así da un true
const isMultiplySeven = function (number){
    if (number % 7 === 0){
        return true;
    } else {
        return false;
    }
}

//Determina si un número es múltiplo de 11, si es así da un true
const isMultiplyEleven = function (number){
    if (number % 11 === 0){
        return true;
    } else {
        return false;
    }
}

//Determina si un numero es cuadrado perfecto, si es así da un true
const isPerfectSquare = function (number){
    const square = number ** (1/2);
    if (square - Math.trunc(number) === 0){
        return true;
    } else {
        return false
    }
}

//Determina si un numero verifica cualquiera de las funciones anteriores, si no es así, se dice que pasa la criba y da un true
const applySieve = function (number){
    if (isEven(number) || isMultiplyFive(number) || isMultiplyThree(number)){
        return false;
    } else if (isMultiplySeven(number) || isMultiplyEleven(number)){
        return false;
    } else if(isPerfectSquare(number)){
        return false;
    }else {
        return true;
    }

}

//determina si un numero es divisible entre otro por cocientes
const isDivisible = function(numerator,quotient){
    if(numerator % quotient === 0){
        return true;
    } else{
        return false;
    }
}

//determina si un numero es primo por cocientes
const isPrimeNumber = function(number){
    let result = false;
    for (let i=(number-1) ; i>1; i--){
        if (isDivisible(number, i)){
            result = false;
            break;
        } else {
            result = true;
        }
    }
    return result;
}

//determina los numeros primos menores a uno dado, excluyendo primero por criba y luego por cocientes
const createSievePrimeNumber = function (number){
    for (let i = number-1; i > 2; i--){
        if (applySieve(i)){
            console.log(i);
        } else{
            if (isPrimeNumber(i)){
                console.log(i);
            }
        }
    }
}

//ejecución
const numero = 1000;

createSievePrimeNumber(numero);