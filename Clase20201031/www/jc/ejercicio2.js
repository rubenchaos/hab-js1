'use strict';

let text = 'Tres tristes tigres tragan trigo en un trigal.'
let arrayText1 = text.split('');
let contador = 0;

//contar el número de letras r
for (let i = 0; i < arrayText1.length; i++){
    if (arrayText1[i] === 'r'){
        contador++
    }
}
console.log(`Número de letras r = ${contador}`);

//contar el número de letras t sean mayusculas o minusculas
contador = 0
let arrayText2 = text.toLocaleLowerCase().split('');
for (let i = 0; i < arrayText1.length; i++){
    if (arrayText1[i] === 't'){
        contador++
    }
}
console.log(`Número de letras t = ${contador}`);

let newText = text.replaceAll('e','i');
console.log(newText);