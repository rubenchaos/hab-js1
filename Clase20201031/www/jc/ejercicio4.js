'use strict';

function findEnemy(array){
    let myPosition = 0;
    let enemyPosition = 0;
    let distance = array.length;
    for (let i = 0; i < array.length -1; i++){
        if(array[i]===1){
            myPosition = i;
        }
    }

    for (let i = 0; i < array.length -1; i++){
        if(array[i]===2){
            if(Math.abs(i-myPosition) < distance){
                distance = Math.abs(i-myPosition);
            }
        }
    }
    return distance;
}

let array1 = [0, 1, 0, 0, 2, 2];
let array2 = [2, 0, 0, 2, 1, 0, 2];

console.log(findEnemy(array1));
console.log(findEnemy(array2));