'use strict';

const abcString = 'ABCDEFGHIJKLMNOPQRSTUWXTZ';
let newArray = [];
let newString = "";

let position = 0

while(position > -1){
    position = parseInt(prompt("Dime la posión de la letra que quieres agregar (de 0 a 25)"));

    if (position > 26){
        alert("Valor no correcto, vuelve a intentarlo");
    } else{
        newArray.push(abcString.charAt(position));
    }
}
newString = newArray.join('');
console.log(`Tu cadena es: ${newString}`);