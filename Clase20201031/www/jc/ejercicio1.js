'use strict';

//ordenar un vector de mayor a menor


const array = [4, 10, 7, 1, 2];
let arrayLength = array.length;
let buffer ='';
let posMin

console.log(`Array antes de ordenar ${array}`);

    for (let i = 0; i < array.length; i++){
        posMin = i;
        buffer = array [i];
        for (let j = i+1; j < array.length; j++){
            if (array[j]<array[posMin]){
                posMin = j;
            }
        }
        array[i] = array[posMin];
        array[posMin]=buffer;
    }

console.log(`Array después de ordenar ${array}`);