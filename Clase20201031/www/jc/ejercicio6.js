'use strict';


function isPalindrome(string){
    const array1 = string.replaceAll(' ',"").toLocaleLowerCase().split("");
    const array2 = string.replaceAll(' ',"").toLocaleLowerCase().split("").reverse();

    let mark = -1;
    for (let i = 0; i < array1.length; i++){
        if(array1[i] === array2[i]){
            mark = 1;
        } else{
            mark = 0;
            break;
        }
    }

    if (mark === 1){
        console.log("El string es palíndromo");
    } else {
        console.log("El string no es palíndromo")
    }

    // return array1 === array2;
}

const myString = 'Arriba la birra';

isPalindrome(myString);
