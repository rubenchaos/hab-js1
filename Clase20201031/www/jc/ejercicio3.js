'use strict';

let text = 'O Vicedo;Santiago;Foz;O Grove;Ribadeo;Chantada;Sarria;Ponteareas';

//dividir el string en un array
let arrayText = text.split(';');
console.log(arrayText);

//agregar ubicaciones en index 1 y 5
arrayText.splice(1,0,"Miño");
arrayText.splice(5,0,"Carballo");
console.log(arrayText);

//eliminar nuevas ubicaciones
arrayText.splice(1,1);
arrayText.splice(5,1);
console.log(arrayText);