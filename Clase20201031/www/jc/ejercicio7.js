/* #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Dado un array con el siguiente formato [K, r1, r2, r3...] donde K representa el número de mesas
 * en clase y el resto de números representan las mesas ocupadas...
 * 
 * - Crea un programa que devuelva el nº de veces que dos estudiantes se pueden sentar juntos uno 
 *   al lado del otro. Es decir, o bien de izquierda a derecha, o bien uno encima del otro.
 * 
 * - Las filas en clase son de a dos, es decir, la primera mesa se encuentra arriba a la izquierda,
 *   la segunda mesa se encuentra a la derecha de la primera, la tercera mesa se encuentra debajo
 *   de la primera, etc.
 * 
 * - K debe tener un rango entre 2 y 24 y debe ser un nº par. 
 * 
*/

'use strict';

function calculateSeatCombinations(seatsArray){
    let combinations = 0;
    let freeSeats = [];

    if(seatsArray[0]%2 !== 0 && seatsArray[0] !== 0){ //verifica que se da un array de pupitres correcto
        console.log("Vector de pupitres incorrecto");
    } else {
        for (let i=0; i<seatsArray[0];i++){
            freeSeats.push(i+1);                //crea el array con los asientos totales
        }
        for (let i=1; i<=seatsArray.length-1;i++){  //elimina del array de asientos totales los ocupados, dejando una rray de asientos libres
            
            for (let j=0; j<freeSeats.length;j++){

                 if(seatsArray[i] === freeSeats[j]){
                     freeSeats.splice(j,1);
                     break;
                 }
            }
        }
    }
    console.log(freeSeats);
    //calculo de adyacentes

    for (let i=0; i<freeSeats.length-1; i++){
        console.log(`pupitre ${i+1}`);
        if(freeSeats[i] % 2 !== 0){ //en caso de que el número sea impar

            switch(true){
                case freeSeats[i]+1 === freeSeats [i+1]: //mira si está libre el par contiguo
                combinations++;
                console.log(`libre a la derecha: ${combinations}`);
            
                case freeSeats[i]+2 === freeSeats [i+2]: //mira si está libre el impar contiguo
                combinations++;
                console.log(`libre abajo: ${combinations}`);
            }

        } else { //en caso de que el asientos sea par
            if (freeSeats[i]+2 === freeSeats [i+2]){
                combinations++;
                console.log(`libre abajo: ${combinations}`);
            }
        }
    }
return combinations;
}

const seats=[12,3,5,7,10];
console.log(`El número de combinaciones posibles es ${calculateSeatCombinations(seats)}`)