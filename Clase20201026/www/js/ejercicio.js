"use strict";

//obtención de un número aleatorio del 1 al 10 y asignación a una constante
const NUMBERTARGET = Math.ceil(Math.random()*10);

//presentación al jugador
alert("Si quieres desactivar la bomba tienes que adivinar el número que estoy pensando.");
alert("Es un número del 0 al 10 y solo tienes 5 intentos, ¿te atreves?");

//bucle del juego
for (let i=1;i<=5;i++){
    let UserInput = prompt("¿En qué numero estoy pensando?");
    let UserInputNumber = parseInt(UserInput);

    //if de decisiones
    if(UserInputNumber === NUMBERTARGET){
        alert("¡FELICIDADES! Has acertado, la bomba a sido desactivada.");
        break;
    } else if (UserInputNumber < NUMBERTARGET){
        alert(`Lo siento, no has acertado, el número que estoy pensando es más grande, te quedan ${5-i} intentos`);
    } else if(UserInputNumber > NUMBERTARGET){
        alert(`Lo siento, no has acertado, el número que estoy pensando es más pequeño, te quedan ${5-i} intentos`);
    }

    //if de explosion
    if(i === 5){
        alert("Has consumido todas tus oportunidades");
        alert("¡¡¡BOOOOOOMMMMMM!!!");
    }
}