"use strict";

//función que genere un numero aleatorio
const AleatoryNumber = (maxNum) => Math.ceil(Math.random()*maxNum);

//función de comparación
const Comparation = (number1, number2) => number1 === number2


//obtención de un número aleatorio del 1 al 10 y asignación a una constante
const NUMBERTARGET = AleatoryNumber(10);

//presentación al jugador
alert("Si quieres desactivar la bomba tienes que adivinar el número que estoy pensando.");
alert("Es un número del 0 al 10 y solo tienes 5 intentos, ¿te atreves?");

for (let i=1; i<=5;i++){
    let UserInput = prompt("¿En qué numero estoy pensando?");
    let UserInputNumber = parseInt(UserInput);

    if(Comparation(NUMBERTARGET, UserInput)){
        alert('Número correcto.');
        break;
    } else{
        alert('Numero incorrecto, inténtalo de nuevo.')
    }

    if (i===5){
        alert('¡¡BOOOOMMM!')
    }

}