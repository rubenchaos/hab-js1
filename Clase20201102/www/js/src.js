'use strict';

//EJEMPLO BÁSICO DE CALLBACK

function sayHello1(name){
    return `Hola ${name}`;
}

function sayHello2(name){
    return `¡Qué pasa ${name}!`;
}

/**
 * 
 * @param {Function} myCallback 
 * @param {String} name 
 */

function getMessage(myCallback, name){

    const result = myCallback (name);
    return result;
}

console.log(getMessage(sayHello1,'Rubeń'));
console.log(getMessage(sayHello2,'Rubeń'));

const result = getMessage((name) => `Hola ${name}`, 'Paco');
console.log(result);

//métodos JS co callback

