'use strict';

//bucle while

let counter = 0;

while (counter <= 10){
    console.log(counter);
    counter++;
}

//bucle do-while

let num = 10;

do{
    console.log(num);
    num--;
} while (num !== 0)

//objetos en JS

const Person = {//se puede declarar como const si complica se cambia a let
    name: 'Laura',    //propiedad sin poner el tipo dato que es
    age: 37

}

console.log(Person);
console.log(Person.name);
console.log(Person.age);
Person.favouriteNumber = 14; //creamos una nueva propiedad desde exterior del objeto
console.log(Person);
Person.age++; //modificacion de una propiedad desde el exterior
console.log(Person.age);


const personKeys = Object.keys(Person); //devuelve en un array las propiedades de un objeto
console.log(personKeys);

const personValues = Object.values(Person); //devuelve en un array el valor de las propiedades de un objeto
console.log(personValues);

const MyPerson = { ...Person};  //copia Person en MyPerson pero no vincula en la misma meroria
console.log(MyPerson);
console.log(MyPerson === Person);

const MyPerson2 = Person; //MyPerson apuntará a la misma direccion de memoria de Persona
console.log(MyPerson2);
console.log(MyPerson2 === Person);