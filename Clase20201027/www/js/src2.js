'use strict';

const USERNAME = 'Pedro';

//declaracion de una funcion
function SayHello(){
    return 'hello world';
}

//llamada y uso de una función
let myText = SayHello();
console.log(myText);

//función con parametros
function SayHello2(name){
    return `Hello ${name}`;
}

myText = SayHello2('Rubén');
console.log(myText);

myText = SayHello2(USERNAME);
console.log(myText);