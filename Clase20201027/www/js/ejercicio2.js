/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * - Crea una función "showInfo()" a la que le pases como parámetros un nombre y una edad y nos devuelva 
 *   ambos parámetros. Muestra lo que devuelva la función por consola. Aplica lo aprendido sobre "template 
 *   strings" (template literals).
 * 
 * - Llama a la función tres veces con parámetros distintos.
 * 
*/

//primera version de la función
function showInfo(name, age){
    return [name, age];
}

//segunda función de la función
function showInfo2(name, age){
    console.log(`El nombre del usuario es ${name} y se edad es ${age}`);
    // return 1;   
}

//declaramos las constantes que alimentan las funciones
const userName1 = 'Fulano';
const userAge1 = 34;
const userName2 = 'Menagano';
const userAge2 = 33;
const userName3 = 'Zutano';
const userAge3 = 32;

//invocación de la primera función
console.log(`El nombre de usuario es ${showInfo(userName1,userAge1)[0]} y su edad es ${showInfo(userName1,userAge1)[1]}`);
console.log(`El nombre de usuario es ${showInfo(userName2,userAge2)[0]} y su edad es ${showInfo(userName2,userAge2)[1]}`);
console.log(`El nombre de usuario es ${showInfo(userName3,userAge3)[0]} y su edad es ${showInfo(userName3,userAge3)[1]}`);

//invocacion de la segunda función
showInfo2(userName1, userAge1);
showInfo2(userName2, userAge2);
showInfo2(userName3, userAge3);