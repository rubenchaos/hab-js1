'use strict';

const Coche = {
    marca: 'Renault',
    modelo: 'Clio',
    color: 'blanco'
}

console.log(Coche);

Coche.color = 'gris';
Coche.anoMatricula = 2008;

if (confirm(`¿Quieres conocer las propieades de Coche`)){
    console.log(Object.keys(Coche));
} else{
    console.log(Object.values(Coche));
}