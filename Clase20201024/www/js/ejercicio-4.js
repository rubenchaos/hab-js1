//calculadora en base if

//declaración de variables

let numberA = 10;
let numberB = 3;
let opcion = 5;

//calculadora básica
if (opcion === 1) {
    console.log("El resultado de sumar A + B es: "+(numberA+numberB));
} else if (opcion === 2){
    console.log("El resultado de restar A - B es: "+(numberA-numberB));
} else if (opcion === 3){
    console.log("El resultado de multiplicar A * B es: "+(numberA*numberB));
} else if (opcion == 4){
    if (numberB !== 0){
        console.log("El resultado de dividir A / B es: "+(numberA/numberB));
    } else{
        console.log("No se puede dividir por 0");
    }
} else if (opcion === 5) {
    console.log("El resultado de elevar A a B es: "+(numberA**numberB));
} else {
    console.log("Operación no reconocida");
}