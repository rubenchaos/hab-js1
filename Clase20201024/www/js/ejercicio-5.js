//calculadora en base if

//declaración de variables

let numberA = 10;
let numberB = 0;
let opcion = 4;

//calculadora básica
switch (opcion){

    case 1:
    console.log("El resultado de sumar A + B es: "+(numberA+numberB));
    break;

    case 2:
    console.log("El resultado de restar A - B es: "+(numberA-numberB));
    break;

    case 3:
    console.log("El resultado de multiplicar A * B es: "+(numberA*numberB));
    break;

    case 4:
        switch (numberB){
            case 0:
            console.log("No se puede dividir por 0");
            break;

            default:
            console.log("El resultado de dividir A / B es: "+(numberA/numberB));
            break;
        }
        break;

    case 5:
    console.log("El resultado de elevar A a B es: "+(numberA**numberB));
    break;

    default:
    console.log("Operación no reconocida");
    break;
}
