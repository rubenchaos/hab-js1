'use strict';

//ejercicio básico con condicionales base if, revisar

let age = 150;

if (typeof age === 'number' && 0 < age < 100) {

    if (age < 18) {
        console.log("La persona tiene menos de 18 años");
    } else if(18 <= age < 45){
        console.log("La persona tiene entre 18 y 45 años");
    } else {
        console.log("La persona es mayor de 45 años");
    }

} else {
    console.log("Valor no correcto")
}