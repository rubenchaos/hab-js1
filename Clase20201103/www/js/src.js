'use strict';

//método map

const colors = ['amarillo', 'azul', 'negro'];

/**
 * 
 * @param {apunta a cada uno de los valores} value
 * @param {apunta a los indices de cada elemento} index
 * @param {apunta al array como tal} array
 */


//callback sobre map en dos pasos con function arrow
const mapFunction = (value, index,array) => console.log(value, index, array);

colors.map(mapFunction);

//callback sobre map en 1 paso con function arrow
colors.map((value, index,array) => console.log(value, index, array));

//map retorna un array

const nums = [2,4,3,2,7,6,13,24];
console.log(nums);
        //retorna todos los números al doble
const double = nums.map((value) => value * 2);

console.log(double);

        //los numeros en posicion par se multipican por dos
const evenDouble = nums.map((value,index) => {
    if (index % 2 === 0){
        return value*2;
    } else{
        return value;
    }
});

console.log(evenDouble);

        //los numeros pares retornan al dobel
    const evenDouble2 = nums.map((value) => {
        if (value % 2 === 0){
            return value*2;
        } else{
            return value;
        }
        });
        
        console.log(evenDouble2);


//dados dos arrays de items y precios, crear un array de objetos que acumule cada item con su precio considerando que
//la relación entre ambos arrays es a través de su index.

const itemNames = ['calcetin', 'pantalón', 'camiseta'];
const itemPrices = [5,18,29];

const clothes = itemNames.map( (value, index) => {
        return{
        name: value,
        price: itemPrices[index]
    }
} );

console.log(clothes);

//

const nums2 = [3,2,5];

const double2 = nums2.map((value, index, array) => {
    return array [index] = value * 2;
});
console.log(nums2);
console.log(double2);

//ejemplo de filter para obtener un array con numeros mayores que 10 de un array dado

const numeros = [3, 2, 5, 10, 34, 12, 35, 11]

const greaterThan10 = numeros.filter((value, index, array) => value >10); //despues del arrow va la condición de filtado

console.log(greaterThan10);