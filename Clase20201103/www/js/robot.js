//crear un objeto Robot con métodos moveUp, moveDown, moveRight, moveLeft para que se desplace por una matriz


'use strict';

const Robot = {
    name: 'Mr. Robot',
    column: 0,
    row: 0,
    moveUp: function(board){
        if(Robot.row === 0){
            console.log('No puedes moverte hacia arriba.');
        } else{
            console.log('Cambiando posición.');
            Robot.row--;
        }
    },
    moveDown: function(board){
        if(Robot.row === 2){
            console.log('No puedes moverte hacia abajo.');
        } else{
            console.log('Cambiando posición.');
            Robot.row++;
        }
    },
    moveRight: function(board){
        if(Robot.column === 2){
            console.log('No puedes moverte hacia la derecha.');
        } else{
            console.log('Cambiando posición.');
            Robot.column++;
        }
    },
    moveLeft: function(board){
        if(Robot.column === 0){
            console.log('No puedes moverte hacia la izquierda.');
        } else{
            console.log('Cambiando posición.');
            Robot.column--;
        }
    },
    showPosition: function(board){
        console.log(`Tu posición es: fila ${Robot.row} y columna ${Robot.column}. La letra almacenada es ${board[Robot.row][Robot.column]}.`);
    }
}


const testBoard =[
    ['A','B','C'],
    ['D','E','F'],
    ['G','H','I']
]

Robot.moveDown(testBoard);
Robot.showPosition(testBoard);
Robot.moveDown(testBoard);
Robot.showPosition(testBoard);
Robot.moveRight(testBoard);
Robot.showPosition(testBoard);
Robot.moveRight(testBoard);
Robot.showPosition(testBoard);
Robot.moveUp(testBoard);
Robot.showPosition(testBoard);
Robot.moveUp(testBoard);
Robot.showPosition(testBoard);
Robot.moveLeft(testBoard);
Robot.showPosition(testBoard);
Robot.moveLeft(testBoard);
Robot.showPosition(testBoard);
Robot.moveLeft(testBoard);
Robot.showPosition(testBoard);